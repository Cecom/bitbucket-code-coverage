package com.atlassian.bitbucket.coverage.listeners;

import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.coverage.CoverageManager;
import com.atlassian.bitbucket.event.pull.PullRequestDeclinedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestDeletedEvent;
import com.atlassian.bitbucket.event.pull.PullRequestMergedEvent;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.event.api.EventListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;

public class PullRequestListener {

    private final CommitService commitService;
    private final CoverageManager coverageManager;
    private final SecurityService securityService;
    private final ExecutorService executorService;

    public PullRequestListener(CommitService commitService, CoverageManager coverageManager,
                               SecurityService securityService, ExecutorService executorService) {
        this.commitService = commitService;
        this.coverageManager = coverageManager;
        this.securityService = securityService;
        this.executorService = executorService;
    }

    @EventListener
    public void onPullRequestDeclined(PullRequestDeclinedEvent event) {
        cleanupCoverage(event.getPullRequest());
    }

    @EventListener
    public void onPullRequestDeleted(PullRequestDeletedEvent event) {
        cleanupCoverage(event.getPullRequest());
    }

    @EventListener
    public void onPullRequestMerged(PullRequestMergedEvent event) {
        cleanupCoverage(event.getPullRequest());
    }

    private void cleanupCoverage(PullRequest pullRequest) {
        executorService.execute(() ->
                securityService.escalate("Clean up coverage")
                        .withPermission(pullRequest.getFromRef().getRepository(), Permission.REPO_READ)
                        .withPermission(pullRequest.getToRef().getRepository(), Permission.REPO_READ)
                        .call(() -> {
                            commitService.streamCommitsBetween(new CommitsBetweenRequest.Builder(pullRequest).build(),
                                    commit -> {
                                        coverageManager.deleteCoverage(commit.getId());
                                        return true;
                                    });
                            return null;
                        })
        );
    }
}
