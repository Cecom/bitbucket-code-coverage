package com.atlassian.bitbucket.coverage;

import org.junit.Test;

import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CoverageParserTest {

    private CoverageParser coverageParser = new CoverageParser();

    @Test
    public void nullStringIsNotValidCoverage() {
        assertThat(coverageParser.isValidCoverage(null), is(false));
    }

    @Test
    public void emptyStringIsNotValidCoverage() {
        assertThat(coverageParser.isValidCoverage(""), is(false));
    }

    @Test
    public void noCoverageLinesIsNotValidCoverage() {
        assertThat(coverageParser.isValidCoverage("C:;"), is(false));
    }

    @Test
    public void everyLineNumberIsValidInt() {
        assertThat(coverageParser.everyLineNumberIsValidInt(new String[]{"1", "2"}), is(true));
    }

    @Test
    public void invalidLineNumber() {
        assertThat(coverageParser.everyLineNumberIsValidInt(new String[]{"1", "a"}), is(false));
    }

    @Test
    public void invalidCoverageTypeInCoverageEntry() {
        assertThat(coverageParser.hasDataInCoverageEntry("Z:1"), is(false));
    }

    @Test
    public void noLinesInCoverageEntry() {
        assertThat(coverageParser.hasDataInCoverageEntry("C"), is(false));
    }

    @Test public void hasLineCoveredInCoverageEntry() {
        assertThat(coverageParser.hasDataInCoverageEntry("C:1,2"), is(true));
    }

    @Test
    public void parseCoverageDoesntDropEmptyCoverage() {
        Map<String, Set<String>> result = coverageParser.parseCoverage("C:1;P:;U:2");

        assertThat(result.size(), is(3));
    }

    @Test
    public void coverageToLinesReturnsEmptySetFromNull() {
        Set<String> result = coverageParser.coverageToLines(null);

        assertThat(result, empty());
    }

    @Test
    public void coverageToLinesReturnsEmptySetFromEmptyString() {
        Set<String> result = coverageParser.coverageToLines("");

        assertThat(result, empty());
    }

    @Test
    public void coverageToLinesReturnsSetOfLines() {
        Set<String> result = coverageParser.coverageToLines("1,2,3");

        assertThat(result, hasSize(3));
    }

    @Test
    public void linesToCoverage() {
        String result = coverageParser.linesToCoverage(newHashSet("1", "2"));
        assertNotNull(result);
    }

    @Test
    public void parseCoverage() {
        Map<String, Set<String>> result = coverageParser.parseCoverage("", "", "");
        assertThat(result.size(), is(3));
    }

}
