package com.atlassian.bitbucket.coverage;

import com.atlassian.bitbucket.coverage.dao.AoFileCoverage;
import com.atlassian.bitbucket.coverage.dao.CodeCoverageDao;
import com.atlassian.bitbucket.coverage.rest.CommitCoverageEntity;
import com.atlassian.bitbucket.coverage.rest.FileCoverageEntity;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.defaultString;

@Component
public class CoverageManager {

    private final CodeCoverageDao codeCoverageDao;
    private final CoverageParser coverageParser;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public CoverageManager(CodeCoverageDao codeCoverageDao,
                           CoverageParser coverageParser,
                           @Qualifier("transactionTemplate")
                           TransactionTemplate transactionTemplate) {
        this.codeCoverageDao = codeCoverageDao;
        this.coverageParser = coverageParser;
        this.transactionTemplate = transactionTemplate;
    }

    public List<FileCoverageEntity> saveCoverage(
            String commitId, CommitCoverageEntity commitCoverageEntity) throws InvalidCoverageException {

        List<AoFileCoverage> result = new LinkedList<>();
        if (null == commitCoverageEntity.getFiles()) {
            return entityFromCoverage(result);
        }

        validateCoverageEntities(commitCoverageEntity.getFiles());

        transactionTemplate.execute(() -> {
            for (FileCoverageEntity fileCoverageEntity : commitCoverageEntity.getFiles()) {
                AoFileCoverage fileCoverage = codeCoverageDao.addFileCoverage(
                        commitId,
                        fileCoverageEntity.getPath(),
                        fileCoverageEntity.getCoverage()
                );

                result.add(fileCoverage);
            }
            return null;
        });

        return entityFromCoverage(result);
    }

    public FileCoverageEntity getCoverage(String commitId, String path) {
        AoFileCoverage result = transactionTemplate.execute(
                () -> codeCoverageDao.findFileCoverage(commitId, path));
        return entityFromCoverage(result);
    }

    public int deleteCoverage(String commitId) {
        return transactionTemplate.execute(
                () -> codeCoverageDao.deleteCoverageForCommit(commitId)
        );
    }

    protected void validateCoverageEntities(List<FileCoverageEntity> fileCoverageEntities) {
        List<FileCoverageEntity> invalidEntities = fileCoverageEntities
                .stream()
                .filter(fileCoverageEntity -> !coverageParser.isValidCoverage(fileCoverageEntity.getCoverage()))
                .collect(Collectors.toList());
        if (!invalidEntities.isEmpty()) {
            throw new InvalidCoverageException(
                    "Invalid or empty coverage data was supplied for one or more files.", invalidEntities);
        }
    }

    protected List<FileCoverageEntity> entityFromCoverage(List<AoFileCoverage> fileCoverageList) {
        if (null == fileCoverageList) {
            return null;
        }
        List<FileCoverageEntity> result = new ArrayList<>(fileCoverageList.size());
        for (AoFileCoverage fileCoverage: fileCoverageList) {
            result.add(entityFromCoverage(fileCoverage));
        }
        return result;
    }

    protected FileCoverageEntity entityFromCoverage(AoFileCoverage fileCoverage) {
        if (null == fileCoverage) {
            return null;
        }

        FileCoverageEntity result = new FileCoverageEntity();
        result.setPath(fileCoverage.getPath());
        String coverage = String.format(
            "C:%s;P:%s;U:%s",
            defaultString(fileCoverage.getCovered()),
            defaultString(fileCoverage.getPartlyCovered()),
            defaultString(fileCoverage.getUnCovered()));
        result.setCoverage(coverage);
        return result;
    }

}
