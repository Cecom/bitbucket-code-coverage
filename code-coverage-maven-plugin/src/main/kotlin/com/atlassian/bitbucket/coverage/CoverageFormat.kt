package com.atlassian.bitbucket.coverage

import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter
import com.atlassian.bitbucket.coverage.converter.CoberturaConverter
import com.atlassian.bitbucket.coverage.converter.JacocoConverter
import com.atlassian.bitbucket.coverage.converter.LcovConverter

enum class CoverageFormat(val converter: AbstractCoverageConverter) {
    LCOV(LcovConverter()),
    JACOCO(JacocoConverter()),
    COBERTURA(CoberturaConverter());
}
