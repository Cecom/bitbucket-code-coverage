define('feature/coverage/diff-coverage', [
        'aui',
        'jquery',
        'lodash',
        'bitbucket/internal/feature/file-content/diff-view-file-types',
        'bitbucket/internal/util/client-storage',
        'bitbucket/internal/util/events',
        'bitbucket/internal/util/function',
        'bitbucket/internal/util/promise',
        'bitbucket/util/server',
        'bitbucket/util/state',
        'exports'
    ],
    function (AJS,
              $,
              _,
              DiffViewFileType,
              clientStorage,
              events,
              fn,
              promiseUtil,
              server,
              pageStateApi,
              exports) {

        'use strict';

        var lastLoadContext = {};
        var diffView;
        var diffViewReady = false;
        var coverageInitialized = false;

        var coverState = {
            'default': 'unknown',
            'U': 'uncovered',
            'P': 'partial',
            'C': 'covered'
        };

        /**
         * Initialize!
         *
         * This returns true as we use this function in the <client-condition> for the client web panel.
         * @returns {boolean}
         */
        function init() {
            registerDiffChangeListener();
            return true;
        }

        function reset() {
            diffView = null;
            lastLoadContext.canceled = true;
            diffViewReady = false;
        }

        /**
         * Some things that need to be set up the first time we're showing coverage in a page lifecycle
         */
        var setupOnceWhenWebItemReady = _.once(function setupOnce() {
            coverageInitialized = true;
            return true;
        });

        /**
         * A registration function for the diffViewContentChanged event.
         * Wrapping it in `once` ensures it does not register multiple listeners during our lifecycle
         */
        var registerDiffChangeListener = _.once(function () {
            var register = function(change) {
                diffView = change.view;

                setupOnceWhenWebItemReady();
                diffViewReady = true;

                diffView.on('destroy', reset);

                lastLoadContext.canceled = true;
                lastLoadContext = load();

                diffView.registerGutter('line-coverage', {weight: 1400, fileType: DiffViewFileType.TO});
                var $dummy = $(bitbucket.coverage.indicator({coverState: 'placeholder'}));

                change.eachLine(function (data) {
                    diffView.setGutterMarker(data.handles.TO || data.handles.FROM || data.handles.SOURCE, 'line-coverage', $dummy.clone().get(0));
                });

                $('.file-content').addClass('bitbucket-coverage-ready');
            };
            events.on('bitbucket.internal.feature.fileContent.sourceViewContentLoaded', register);
            events.on('bitbucket.internal.feature.fileContent.diffViewContentLoaded', register);
        });

        /**
         * Check the Pull Request or Changeset and return the commit ID
         *
         * @returns {string}
         */
        function getLatestCommitId() {
            var pullRequest = pageStateApi.getPullRequest();
            var commit = pageStateApi.getCommit();
            var ref = pageStateApi.getRef()
            if (pullRequest && pullRequest.fromRef) {
                return pullRequest.fromRef.latestCommit;
            } else if (commit && commit.id) {
                return commit.id;
            } else if (ref) {
                return ref.latestCommit;
            }
        }

        /**
         * Add the coverage information to the diff view.
         *
         * @param {object} coverageData
         */
        function decorateCoverage(coverageData) {
            if (!coverageData) {
                return;
            }

            var coverage = coverageData.coverage;

            if (!coverage) {
                return;
            }

            if (!diffView || !diffView.operation) {
                return;
            }

            diffView.operation(function () {
                _.forEach(coverage.split(';'), function (coverageInfo) {

                    var covered = coverageInfo.split(':')[0];
                    var lineNumbers = coverageInfo.split(':')[1].split(',');

                    lineNumbers.forEach(function (lineNumber) {
                        var lineHandle = diffView.getLineHandle({
                            lineNumber: lineNumber,
                            lineType: 'ADDED',
                            fileType: 'TO'
                        }) || diffView.getLineHandle({
                            lineNumber: lineNumber,
                            lineType: 'CONTEXT'
                        });

                        if (!lineHandle) {
                            return;
                        }

                        var renderedTemplate = bitbucket.coverage.indicator({
                            coverState: (coverState[covered] || coverState['default']).toLowerCase()
                        });

                        diffView.setGutterMarker(lineHandle, 'line-coverage', $(renderedTemplate)[0]);
                    });

                });
            });

        }

        /**
         * Load coverage for a file.
         *
         * @returns {object} - context
         */
        function load() {
            var ctx = {
                canceled: false,
                hasContext: function () {
                    return this.cid && this.path;
                }
            };

            var filePathObj = pageStateApi.getFilePath();

            // If we do not have a filepath object to extract a path from then bail out early.
            if (!(filePathObj && filePathObj.components && filePathObj.components.length)) {
                // make sure that an object is returned so usages of "context" don't get messed up.
                return {};
            }

            var filePath = filePathObj.components.join('/');

            function checkCancel(data) {
                return (ctx.canceled ? rejected("cancel") : data);
            }

            promiseUtil.waitFor({
                name: "coverageInitialized",
                interval: 10,
                predicate: function () {
                    // coverageInitialized is set when the diffViewContentLoaded event fires and triggers some initialization
                    return coverageInitialized === true && diffViewReady === true;
                }
            })
                .then(checkCancel)
                .then(
                    function () {
                        ctx.cid = getLatestCommitId();
                        ctx.path = filePath;
                        if (ctx.hasContext()) {
                            return loadCoverage(ctx.cid, ctx.path);
                        } else {
                            return rejected('No context to make request');
                        }
                    }
                )
                .then(checkCancel)
                .then(decorateCoverage)
                .done(Chaperone.checkFeatureVisibility)
                .fail(function (error) {
                    // Turn on for debugging.
                    //console.log("load ", ctx.path, " error: ", error);
                });

            return ctx;
        }

        /**
         * Reject a promise with given value
         * @param {*} value
         * @returns {Promise} - a rejected promise
         */
        function rejected(value) {
            return $.Deferred().reject(value);
        }

        /**
         * Load coverage for a given commit and path.
         *
         * @param {string} cid - commit ID
         * @param {string} path - File Path
         * @returns {*}
         */
        function loadCoverage(cid, path) {

            function rejectingErrorHandler(xhr, textStatus, errorThrown, data) {
                return rejected('Error ' + xhr.status + ': ' + JSON.stringify(data.errors || {}));
            }

            return server.rest({
                url: AJS.contextPath() + "/rest/code-coverage/1.0/commits/" + cid + "?path=" + encodeURIComponent(path),
                statusCode: {
                    '200': false,
                    '404': rejectingErrorHandler
                }
            });
        }

        exports.init = init;

    });